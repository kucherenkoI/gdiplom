package main;

import searchers.AlphaSearcher;
import searchers.CSearchers;


public class Main {

    public static void main(String[] args) {
        double alpha = AlphaSearcher.search();
        System.out.println("alpha: " + alpha);

        double c = CSearchers.calculateC(alpha);
        System.out.println("c:  " + c);

    }


}
