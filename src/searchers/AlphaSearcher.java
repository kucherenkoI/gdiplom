package searchers;

import static java.lang.Math.pow;
import static shared.Data.*;
import static shared.Data.array;

public class AlphaSearcher {

    private static double a = 0.5;

    public static double search() {
        a = 0.943292;
        System.out.println("result: " + evalExpression());
        return a;
    }

    private static double evalExpression() {
        return a * middle() - r;
    }


    private static double middle() {

        return firstMember() + secondMember() - sumLnT();
    }

    private static double secondMember() {
        double result = 0.0;

        double denum = (1d / r) * sumTsq() + (N - r) / r * pow(T, a);

        result = (1d / denum) * (N - r) * pow(T, a) * Math.log(T);

        return result;
    }

    private static double firstMember() {
        double result = 0.0;

        double denum = (1d / r) * sumTsq() + ((N - r) / r ) * pow(T, a);

        result = (1d / denum) * sumTsqLnT();

        return result;
    }


    private static double sumTsq() {
        double sum = 0.0;
        for(int i = 0; i < r; i++) {
            sum += pow(array[i], a);
        }
        return sum;
    }


    private static double sumTsqLnT() {
        double sum = 0.0;
        for(int i = 0; i < r; i++) {
            sum += pow(array[i], a) * Math.log(array[i]);
        }
        return sum;
    }


    private static double sumLnT() {
        double sum = 0.0;
        for(int i = 0; i < r; i++) {
            sum += Math.log(array[i]);
        }
        return sum;
    }



}
