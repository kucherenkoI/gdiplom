package searchers;


import static java.lang.Math.pow;
import static shared.Data.*;

public class CSearchers {


    private static double mAlpha;

    public static double calculateC(double alpha) {
        mAlpha = alpha;

        double c;

        double denum = (1d / r) * sumTsq() + ((N - r) / r ) * pow(T, mAlpha);

        c = 1d / denum;

        return c;
    }

    private static double sumTsq() {
        double sum = 0.0;
        for(int i = 0; i < r; i++) {
            sum += pow(array[i], mAlpha);
        }
        return sum;
    }

}
